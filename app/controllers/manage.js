var ManageController = Ember.ArrayController.extend({
  actions: {
    addNew: function () {
      console.log("~~~~~~~~~~~~~ CONTROLLER -> MANAGE -> ACTION -> ADD-NEW ~~~~~~~~~~~~~~~");
        //this.transitionToRoute('edit.site');
        this.transitionToRoute('edit.new');
    }
  }
});

export default ManageController;
